import React, { useState } from 'react';
import { Modal, Form, Input, Select, InputNumber, Space, DatePicker } from 'antd';
import moment from 'moment';

const FilmsModal = props => {
  const { currentModalData, isModalOpen, toggleModal, handleModal } = props;

  const [form] = Form.useForm();
  const [libraryYear, setLibraryYear] = useState(moment().year(currentModalData.year));
  const [libraryRelaseDate, setLibraryReleaseDate] = useState(moment(currentModalData.releaseDate));

  const movieOrSeriesList = [{
    id: 'movie',
    name: 'movie',
  }, {
    id: 'series',
    name: 'series',
  }];

  const languagesList = [{
    id: 'en',
    name: 'en',
  }, {
    id: 'fr',
    name: 'fr',
  }];

  const handleRelaseDateChange = (value, dateString) => {
    setLibraryReleaseDate(dateString);
  };
  const handleYearChange = (value, dateString) => {
    setLibraryYear(dateString);
  };
  const handleUpdateModal = formValues => {
    debugger;
    formValues.releaseDate = libraryRelaseDate;
    formValues.year = libraryYear;
    const libraryUpdateModalObj = {
      filter: { _id: currentModalData._id },
      updateDocument: formValues
    };

    handleModal(libraryUpdateModalObj);
    toggleModal();
  }

  return (<Modal
    layout="vertical"
    title="Update Library"
    okText="Update"
    cancelText="Cancel"
    visible={isModalOpen}
    onOk={() => {
      form
        .validateFields()
        .then((values) => {
          handleUpdateModal(values)
        }).catch((err) => console.log("Validation Error: ", err))
    }}
    onCancel={() => toggleModal()}
  >
    <Form
      form={form}
      initialValues={{
        _id: currentModalData._id,
        title: currentModalData.title,
        studioName: currentModalData.studioName,
        studioId: currentModalData.studioId,
        MovieOrSeries: currentModalData.MovieOrSeries,
        releaseDate: currentModalData.releaseDate,
        year: currentModalData.year,
        ratings: currentModalData.ratings,
        runtime: currentModalData.runtime,
        language: currentModalData.language,
        format: currentModalData.format,
        genre: currentModalData.genre,
        cast: currentModalData.cast,
      }}
    >
      <Form.Item
        label="ID"
        name="_id"
      >
        <Input disabled />
      </Form.Item>
      <Form.Item
        label="Title"
        name="title"
      >
        <Input placeholder="Library Id" />
      </Form.Item>
      <Form.Item
        label="Studio Name"
        name="studioName"
      >
        <Input placeholder="Studio Name" />
      </Form.Item>
      <Form.Item
        label="Studio Id"
        name="studioId"
      >
        <Input placeholder="Studio Id" />
      </Form.Item>
      <Form.Item
        label="Movie Or Series"
        name="MovieOrSeries"
      >
        <Select
          placeholder="Movie Or Series"
          options={movieOrSeriesList.map(ms => ({ label: ms.name, value: ms.id }))}
        />
      </Form.Item>
      <Form.Item
        label="Release Date"
        name="releaseDate"
      >
        <Space direction="vertical">
          <DatePicker
            value={moment(libraryRelaseDate)}
            onChange={handleRelaseDateChange}
            placeholder="Release Date"
            allowClear={false}
            inputReadOnly />
        </Space>
      </Form.Item>
      <Form.Item
        label="Year"
        name="year"
      >
        <Space direction="vertical">
          <DatePicker
            value={moment(libraryYear)}
            onChange={handleYearChange}
            picker="year"
            placeholder="Year"
            allowClear={false}
            inputReadOnly />
        </Space>
      </Form.Item>
      <Form.Item
        label="Ratings"
        name="ratings"
      >
        <Input placeholder="Ratings" />
      </Form.Item>
      <Form.Item
        label="Runtime"
        name="runtime"
      >
        <InputNumber min={0} placeholder="Runtime" />
      </Form.Item>
      <Form.Item
        label="Language"
        name="language"
      >
        <Select
          options={languagesList.map(l => ({ label: l.name, value: l.id }))}
        />
      </Form.Item>
      <Form.Item
        label="Format"
        name="format"
      >
        <Input placeholder="Format" />
      </Form.Item>
      <Form.Item
        label="Genre"
        name="genre"
      >
        <Input placeholder="Genre" />
      </Form.Item>
      <Form.Item
        label="Cast"
        name="cast"
      >
        <Input placeholder="Cast" />
      </Form.Item>
    </Form>
  </Modal>)
};

export default FilmsModal;