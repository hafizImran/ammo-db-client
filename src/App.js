import React, { useEffect, useState } from 'react';
import { Form, Input, Button, Layout, Menu, Breadcrumb, Popover, Table, Upload, Popconfirm } from 'antd';
import { UserOutlined, UploadOutlined, FormOutlined, CloseOutlined } from '@ant-design/icons';
import { getPlatforms, updatePlatform, insertPlatforms, deletePlatform, searchPlatformById } from './routes/platformRoutes';
import { getLibraries, searchLibraryById, insertLibrary, updateLibrary, deleteLibrary } from './routes/libraryRoutes';
import { getFilms, searchFilmsById, insertFilms, updateFilms, deleteFilms } from './routes/filmsRoutes';
import './App.css';
import 'antd/dist/antd.css';
import './common/css/commonStyles.css';
import PlatformModal from './views/platforms/platformModal';
import LibrayModal from './views/library/libraryModal';
import moment from 'moment';


const App = () => {

  const [platformData, setPlatformData] = useState([]);
  const [uploadPopoverVisible, setUploadPopoverVisible] = useState(false);

  const [platformUpdateModalData, setPlatformUpdateModalData] = useState({});
  const [isPlatformUpdateModalOpen, setPlatformUpdateModalOpen] = useState(false);

  const [libraryData, setLibraryData] = useState([]);

  const [libraryUpdateModalData, setLibraryUpdateModalData] = useState({});
  const [isLibraryUpdateModalOpen, setLibraryUpdateModalOpen] = useState(false);

  const [filmsData, setFilmsData] = useState([]);

  const [breadCrumbValue, setBreadCrumbValue] = useState("Platforms");
  const [isLibraryTable, setLibraryTable] = useState(false);
  const [isPlatformTable, setPlatformTable] = useState(true);
  const [isFilmsTable, setFilmsTable] = useState(false);

  const { SubMenu } = Menu;
  const { Header, Content, Sider } = Layout;



  // ------------- Use Effects
  useEffect(() => {
    switch (breadCrumbValue) {
      case "Platforms":
        getPlatforms(setPlatformDataTable);
        break;
      case "Library":
        getLibraries(setLibraryDataTable);
        break;
      case "Films":
        getFilms(setFilmsDataTable);
        break;
      default:
        break;
    }

  }, [breadCrumbValue]);

  // ------------ Handlers
  const setPlatformDataTable = data => {
    setPlatformData(data);
  };

  const setLibraryDataTable = data => {
    setLibraryData(data);
  };

  const setFilmsDataTable = data => {
    setFilmsData(data);
  };

  const handleSearch = searchText => {
    switch (breadCrumbValue) {
      case "Platforms":
        searchPlatformById(searchText.target.value.length > 0 ? searchText.target.value : null, setPlatformDataTable);
        break;
      case "Library":
        searchLibraryById(searchText.target.value.length > 0 ? searchText.target.value : null, setLibraryDataTable);
        break;
      case "Films":
        searchFilmsById(searchText.target.value.length > 0 ? searchText.target.value : null, setFilmsDataTable);
        break;
      default:
        break;
    }

  };

  const handleSubmenuChange = menuItem => {
    setLibraryTable(false);
    setPlatformTable(false);
    setFilmsTable(false);

    switch (menuItem.key) {
      case "1": //  Platform
        setBreadCrumbValue("Platforms");
        setPlatformTable(true);
        break;
      case "2": // Library
        setBreadCrumbValue("Library");
        setLibraryTable(true);
        break;
      case "3": // Films
        setBreadCrumbValue("Films");
        setFilmsTable(true);
        break;

      default:
        break;
    }
  }

  const updatePlatformRecord = record => {
    setPlatformUpdateModalData(record);
    togglePlatformUpdateModal();
  };

  const updateLibraryRecord = record => {
    setLibraryUpdateModalData(record);
    toggleLibraryUpdateModal();
  };



  // ------------- Toggles
  const togglePlatformUpdateModal = () => {
    const action = isPlatformUpdateModalOpen ? false : true;
    setPlatformUpdateModalOpen(action);
  }
  const toggleLibraryUpdateModal = () => {
    const action = isLibraryUpdateModalOpen ? false : true;
    setLibraryUpdateModalOpen(action);
  }

  // ----------------- dispatchers Api

  const handleUpdatePlatformModal = platform => {
    updatePlatform(platform, setPlatformDataTable);
  };
  const deletePlatformRecord = record => {
    deletePlatform(record, setPlatformDataTable);
  };
  const handleUpdateLibraryModal = library => {
    updateLibrary(library, setLibraryDataTable);
  };
  const deleteLibraryRecord = record => {
    deleteLibrary(record, setLibraryDataTable);
  };
  const handleUpdateFilmsModal = film => {
    updateFilms(film, setFilmsDataTable);
  };
  const deleteFilmsRecord = record => {
    deleteFilms(record, setFilmsDataTable);
  };


  // -------------------  Custom Function for UI

  const UploadContent = () => {
    const props = {
      name: 'file',
      accept: '.csv',
      onChange(info) {
        if (info.file.status === 'error') {
          const delimiter = ",";
          const input = info.file.originFileObj;
          const reader = new FileReader();
          reader.onload = function (e) {
            const text = e.target.result;
            const headers = text.slice(0, text.indexOf("\n")).split(delimiter);
            const rows = text.slice(text.indexOf("\n") + 1).split("\n");
            const fileData = rows.map(function (row) {
              const values = row.split(delimiter);
              const el = headers.reduce(function (object, header, index) {
                if (values[index]) {
                  switch (header) {
                    case "rights":
                      const rightsArry = values[index].split("|");
                      const rightsTypeArry = rightsArry[0].split("#");
                      const rightsDateRange = rightsArry[2].split("to");

                      const rightsFinalData = {
                        type: rightsTypeArry,
                        country: rightsArry[1],
                        from: new Date(rightsDateRange[0]),
                        to: new Date(rightsDateRange[1]),
                      }
                      console.log(rightsFinalData);
                      object[header.replace(' ', '')] = rightsFinalData;
                      break;
                    case "cast":
                      object[header.replace(' ', '')] = values[index].split("|");
                      break;
                    case "deliveryStatus":
                      const deliveryStatusArry = values[index].split("|");
                      let deliveryStatusArrayList = [];
                      deliveryStatusArry.forEach(item => {
                        var itemList = item.split("#");
                        itemList.forEach(x => {

                        })
                        deliveryStatusArrayList.push({
                          platform: itemList[0],
                          date: new Date(itemList[1]),
                          country: itemList[2],
                        })
                      })

                      object[header.replace(' ', '')] = deliveryStatusArrayList;
                      break;
                    case "genre":
                      object[header.replace(' ', '')] = values[index].split("|");
                      break;
                    case "producer":
                      object[header.replace(' ', '')] = values[index].split("|");
                      break;
                    default:
                      object[header.replace(' ', '')] = values[index];
                      break;
                  }
                }
                return object;
              }, {});
              return el;
            });

            console.log("updatedData", fileData);

            switch (breadCrumbValue) {
              case "Platforms":
                insertPlatforms(fileData, setPlatformDataTable);
                break;
              case "Library":
                insertLibrary(fileData, setLibraryDataTable);
                break;
              case "Films":
                insertFilms(fileData, setFilmsDataTable);
                break;
              default:
                break;
            }

            setUploadPopoverVisible(!uploadPopoverVisible);
          };
          reader.readAsText(input);
        }
      },
    };

    return (
      <Upload {...props} showUploadList={false}>
        <Button icon={<UploadOutlined />}>Click to Upload</Button>
      </Upload>)
  };

  const platformColumns = [
    {
      title: 'ID',
      dataIndex: '_id',
      key: '_id',
    },
    {
      title: 'Display Name',
      dataIndex: 'platDisplayName',
      key: 'platDisplayName',
    },
    {
      title: 'Operation',
      dataIndex: 'operation',
      render: (text, record) => {
        return (
          <div>
            <Button
              className="edit-btn"
              onClick={() => updatePlatformRecord(record)}
              icon={<FormOutlined />}
            />
            <Popconfirm
              title="Are you Sure to Delete this Record?"
              onConfirm={() => deletePlatformRecord(record)}
              okText="Ok"
              cancleText="Cancel"
            >
              <Button
                className="delete-btn"
                icon={<CloseOutlined />}
              />
            </Popconfirm>

          </div>
        );
      },
    },
  ];

  const libraryColumns = [
    {
      title: 'ID',
      dataIndex: '_id',
      key: '_id',
    },
    {
      title: 'Movie/Series Title',
      dataIndex: 'title',
      key: 'title',
    },
    {
      title: 'Studio Name',
      dataIndex: 'studioName',
      key: 'studioName',
    },
    {
      title: 'Movie Or Series',
      dataIndex: 'MovieOrSeries',
      key: 'MovieOrSeries',
    },
    {
      title: 'Release Date',
      dataIndex: 'releaseDate',
      key: 'releaseDate',
    },
    {
      title: 'Operation',
      dataIndex: 'operation',
      render: (text, record) => {
        return (
          <div>
            <Button
              className="edit-btn"
              onClick={() => updateLibraryRecord(record)}
              icon={<FormOutlined />}
            />
            <Popconfirm
              title="Are you Sure to Delete this Record?"
              onConfirm={() => deleteLibraryRecord(record)}
              okText="Ok"
              cancleText="Cancel"
            >
              <Button
                className="delete-btn"
                icon={<CloseOutlined />}
              />
            </Popconfirm>

          </div>
        );
      },
    },
  ];

  const filmsColumns = [
    {
      title: 'ID',
      dataIndex: '_id',
      key: '_id',
    },
    {
      title: 'Movie/Series Title',
      dataIndex: 'title',
      key: 'title',
    },
    {
      title: 'Studio Name',
      dataIndex: 'studioName',
      key: 'studioName',
    },
    {
      title: 'Movie Or Series',
      dataIndex: 'MovieOrSeries',
      key: 'MovieOrSeries',
    },
    {
      title: 'Release Date',
      dataIndex: 'releaseDate',
      key: 'releaseDate',
    },
    {
      title: 'Year',
      dataIndex: 'year',
      key: 'year',
    },
    {
      title: 'Rating',
      dataIndex: 'rating',
      key: 'rating',
    },
    {
      title: 'Runtime',
      dataIndex: 'runtime',
      key: 'runtime',
    },
    {
      title: 'Language',
      dataIndex: 'language',
      key: 'language',
    },
    {
      title: 'Genre',
      dataIndex: 'genre',
      key: 'genre',
    },
    {
      title: 'Operation',
      dataIndex: 'operation',
      render: (text, record) => {
        return (
          <div>
            <Button
              className="edit-btn"
              onClick={() => updateLibraryRecord(record)}
              icon={<FormOutlined />}
            />
            <Popconfirm
              title="Are you Sure to Delete this Record?"
              onConfirm={() => deleteFilmsRecord(record)}
              okText="Ok"
              cancleText="Cancel"
            >
              <Button
                className="delete-btn"
                icon={<CloseOutlined />}
              />
            </Popconfirm>

          </div>
        );
      },
    },
  ];
  const rightsColumns = [
    {
      title: 'Type',
      dataIndex: 'rights',
      key: 'rights',
      render: (text, row) => {
        return (
          <div>
            {text ? text.type[0] : null}
            <br />
            <span className="rights_table_type_expand">click to expand all types</span>
          </div>
        )
      }
    },
    {
      title: 'Start Date',
      dataIndex: 'rights',
      key: 'rights',
      render: (text, row) => {
        return <div>{text ? moment(text.from).format('MMM DD, YY') : null}</div>
      }
    },
    {
      title: 'End Date',
      dataIndex: 'rights',
      key: 'rights',
      render: (text, row) => {
        return <div>{text ? moment(text.to).format('MMM DD, YY') : null}</div>
      }
    },
    {
      title: 'Country/Territory',
      dataIndex: 'rights',
      key: 'rights',
      render: (text, row) => {
        return <div>{text ? text.country : null}</div>
      }
    },
  ];


  return (
    <Layout>
      <Header className="header" />
      <Layout>
        <Sider width={200} className="site-layout-background">
          <Menu
            mode="inline"
            defaultSelectedKeys={['1']}
            defaultOpenKeys={['sub1']}
            style={{ height: '100%', borderRight: 0 }}
            onClick={handleSubmenuChange}
          >
            <SubMenu
              key="sub1"
              icon={<UserOutlined />}
              title="Ammo Content DB"

            >
              <Menu.Item key="1">Platforms</Menu.Item>
              <Menu.Item key="2">Library</Menu.Item>
              <Menu.Item key="3">Films</Menu.Item>
            </SubMenu>
          </Menu>
        </Sider>
        <Layout style={{ padding: '0 24px 24px' }}>
          <Breadcrumb style={{ margin: '16px 0' }}>
            <Breadcrumb.Item>Home</Breadcrumb.Item>
            <Breadcrumb.Item>{breadCrumbValue}</Breadcrumb.Item>
          </Breadcrumb>
          <Content
            className="site-layout-background"
            style={{
              padding: 24,
              margin: 0,
              minHeight: 280,
            }}
          >
            <Form
              name="basic"
              labelCol={{ span: 8 }}
              wrapperCol={{ span: 16 }}
            >
              <Form.Item>
                <Input.Search placeholder="Search By ID..." allowClear onChange={(text) => handleSearch(text)} />
              </Form.Item>
              <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
                <Popover
                  content={<UploadContent />}
                  title="Click to Upload Csv File"
                  trigger="click"
                  visible={uploadPopoverVisible}
                  onVisibleChange={() => setUploadPopoverVisible(!uploadPopoverVisible)}
                >
                  <Button type="primary" className="import-btn">Upload Csv File</Button>
                </Popover>
              </Form.Item>
            </Form>
            {isLibraryTable ?
              <div>
                <div className="table-heading">Library Level Detail</div>
                <Table
                  columns={libraryColumns}
                  dataSource={libraryData}
                  className="table-height"
                  pagination={false}
                />
              </div>
              :
              isPlatformTable ?
                <div>
                  <div className="table-heading">Platform Level Detail</div>
                  <Table
                    columns={platformColumns}
                    dataSource={platformData}
                    className="table-height"
                    pagination={false}
                  />
                </div>
                :
                isFilmsTable ?
                  <div>
                    <div className="table-heading">Films Level Detail</div>
                    <Table
                      columns={filmsColumns}
                      dataSource={filmsData}
                      className="table-height"
                      pagination={false}
                    />
                    <div className="table-heading">Rights Level Detail</div>
                    <Table
                      columns={rightsColumns}
                      dataSource={filmsData}
                      className="table-height"
                      pagination={false}
                    />
                  </div>
                  : null}

            {isPlatformUpdateModalOpen ?
              <PlatformModal
                currentModalData={platformUpdateModalData}
                isModalOpen={isPlatformUpdateModalOpen}
                toggleModal={togglePlatformUpdateModal}
                handleModal={handleUpdatePlatformModal}
              /> : null}
            {isLibraryUpdateModalOpen ?
              <LibrayModal
                currentModalData={libraryUpdateModalData}
                isModalOpen={isLibraryUpdateModalOpen}
                toggleModal={toggleLibraryUpdateModal}
                handleModal={handleUpdateLibraryModal}
              /> : null}
          </Content>
        </Layout>
      </Layout>
    </Layout>
  )
}

export default App;
