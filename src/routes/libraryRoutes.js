import { message } from 'antd';
import axios from 'axios';
import { CONST_VAR } from '../common/constants/constantVariables';

export const getLibraries = (setLibraryData) => {
  axios.get(CONST_VAR.BASE_URL + "/library/library")
    .then(res => {
      if (res.status === 200) {
        setLibraryData(res.data.data);
      } else {
        return setLibraryData([]);
      }
    });
};

export const searchLibraryById = (searchText, setLibraryData) => {
  axios.get(CONST_VAR.BASE_URL + "/library/serachLibrary/" + searchText)
    .then(res => {
      if (res.status === 200) {
        setLibraryData(res.data.data);
      }
    });
};

export const insertLibrary = (libraryFile, setLibraryData) => {
  axios.post(CONST_VAR.BASE_URL + "/library/importLibrary", { data: libraryFile })
    .then(res => {
      if (res.status === 200) {
        message.success('Library File Inserted successfully');
        getLibraries(setLibraryData);
      } else {
        message.error('Library File Not Inserted!');
      }
    });
};

export const updateLibrary = (libraryData, setLibraryData) => {
  axios.post(CONST_VAR.BASE_URL + "/library/updateLibraryField", { data: libraryData })
    .then(res => {
      if (res.status === 200) {
        message.success('Library Field Updated successfully');
        getLibraries(setLibraryData);
      } else {
        message.error('Library Field Not Updated!');
      }
    });
};

export const deleteLibrary = (libraryId, setLibraryData) => {
  axios.delete(CONST_VAR.BASE_URL + "/library/deleteLibrary", { data: libraryId })
    .then(res => {
      if (res.status === 200) {
        message.success('Library File Inserted successfully');
        getLibraries(setLibraryData);
      } else {
        message.error('Library File Not Inserted!');
      }
    });
};
