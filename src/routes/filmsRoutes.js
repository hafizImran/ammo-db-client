import { message } from 'antd';
import axios from 'axios';
import { CONST_VAR } from '../common/constants/constantVariables';

export const getFilms = (setFilmsData) => {
  axios.get(CONST_VAR.BASE_URL + "/films/films")
    .then(res => {
      if (res.status === 200) {
        setFilmsData(res.data.data);
      } else {
        return setFilmsData([]);
      }
    });
};

export const searchFilmsById = (searchText, setFilmsData) => {
  axios.get(CONST_VAR.BASE_URL + "/films/serachFilms/" + searchText)
    .then(res => {
      if (res.status === 200) {
        setFilmsData(res.data.data);
      }
    });
};

export const insertFilms = (filmsFile, setFilmsData) => {
  axios.post(CONST_VAR.BASE_URL + "/films/importFilms", { data: filmsFile })
    .then(res => {
      if (res.status === 200) {
        message.success('Films File Inserted successfully');
        getFilms(setFilmsData);
      } else {
        message.error('Films File Not Inserted!');
      }
    });
};

export const updateFilms = (filmsData, setFilmsData) => {
  axios.post(CONST_VAR.BASE_URL + "/films/updateFilmsField", { data: filmsData })
    .then(res => {
      if (res.status === 200) {
        message.success('Films Field Updated successfully');
        getFilms(setFilmsData);
      } else {
        message.error('Films Field Not Updated!');
      }
    });
};

export const deleteFilms = (filmsId, setFilmsData) => {
  axios.delete(CONST_VAR.BASE_URL + "/films/deleteFilms", { data: filmsId })
    .then(res => {
      if (res.status === 200) {
        message.success('Films File Inserted successfully');
        getFilms(setFilmsData);
      } else {
        message.error('Films File Not Inserted!');
      }
    });
};
