import { message } from 'antd';
import axios from 'axios';
import { CONST_VAR } from '../common/constants/constantVariables';

export const getPlatforms = (setPlatformData) => {
  axios.get(CONST_VAR.BASE_URL + "/platform/platforms")
    .then(res => {
      if (res.status === 200) {
        setPlatformData(res.data.data);
      } else {
        return setPlatformData([]);
      }
    });
};

export const searchPlatformById = (searchText, setPlatformData) => {
  axios.get(CONST_VAR.BASE_URL + "/platform/serachPlatform/" + searchText)
    .then(res => {
      if (res.status === 200) {
        setPlatformData(res.data.data);
      }
    });
};

export const insertPlatforms = (platformFile, setPlatformData) => {
  axios.post(CONST_VAR.BASE_URL + "/platform/importPlatform", { data: platformFile })
    .then(res => {
      if (res.status === 200) {
        message.success('Platform File Inserted successfully');
        getPlatforms(setPlatformData);
      } else {
        message.error('Platform File Not Inserted!');
      }
    });
};

export const updatePlatform = (platformData, setPlatformData) => {
  axios.post(CONST_VAR.BASE_URL + "/platform/updatePlatformField", { data: platformData })
    .then(res => {
      if (res.status === 200) {
        message.success('Platform Field Updated successfully');
        getPlatforms(setPlatformData);
      } else {
        message.error('Platform Field Not Updated!');
      }
    });
};

export const deletePlatform = (platformId, setPlatformData) => {
  axios.delete(CONST_VAR.BASE_URL + "/platform/deletePlatform", { data: platformId })
    .then(res => {
      if (res.status === 200) {
        message.success('Platform File Inserted successfully');
        getPlatforms(setPlatformData);
      } else {
        message.error('Platform File Not Inserted!');
      }
    });
};
